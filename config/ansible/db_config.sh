#!/bin/sh
cd /home/ec2-user/flask/{{ stack_name }}/flask-app
/home/ec2-user/flask/{{ stack_name }}/env/bin/flask db init
/home/ec2-user/flask/{{ stack_name }}/env/bin/flask db migrate
/home/ec2-user/flask/{{ stack_name }}/env/bin/flask db upgrade