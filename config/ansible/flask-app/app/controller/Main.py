import socket
from flask import Flask, jsonify, request, render_template
from app import app


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")

@app.route("/node")
def return_hostname():
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr
    return "This is an example wsgi app served from {} to {}".format(socket.gethostname(), ip)

@app.route("/status")
def health():
    return "running"