class DbConfig:
    def __init__(self):
        self.DB_USERNAME = "{{ db_user }}"
        self.DB_PASSWORD = "{{ db_pass }}"
        self.DB_HOST = "{{ hostvars['localhost']['db_info']['instances'][0]['endpoint']['address'] }}"
        self.DB_PORT = 3306
        self.DB_NAME = "DB{{ stack_name }}"

    def getUri(self):
        return "mysql+mysqlconnector://{}:{}@{}:{}/{}".format(self.DB_USERNAME, self.DB_PASSWORD, self.DB_HOST,
                                                              self.DB_PORT, self.DB_NAME)
