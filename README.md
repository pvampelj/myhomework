# Python app that uses cloudformation and ansible to deploy an flask application to AWS

---

## App description

The MyApp.py is an Python3 script that leverages boto3 to deploy an "ansible-stack" on AWS, after that it copies the content from the config/ansible directory to the EC2 instance running on AWS and executes an ansible command in order to provision and configure the application on AWS. All the outputs (ssh keys, console log file) from the script are stored in the outputs directory. The script can be used to provision the app and then to update it if you did any changes and wish to deploy them.

When started the script will create a new EC2 instance and allow SSH access only from your public IP. After a successful creation it will transfer the files from config/ansible to the ec2-user home directory in AWS and execute deploy.yml ansible-playbook. Ansible will provision all the required infrastructure on AWS for the app and configure all the instances. During the provisioning of the application the script creates a new VPC with 4 subnets (2 private and 2 public), deploys and provisions 2 EC2 instances in different public subnets, configures a RDS MySQL database in the private subnet and create an application load balancer in front of the EC2 instances. All the EC2 instances use the latest Amazon Linux2 AMI and are of the type t2.micro.

After a successful execution you will be provided with the link to your application.
---

## Requirements

### Local system requirements:

1. Python3
2. boto3 
3. paramiko

### AWS Requirements:

1. AWS account with sufficient permissions to create (cloudformation stacks, EC2 instances, VPC, subnets, ELB target groups, ELB, RDS databases, security groups)
2. AWS Access Key
3. AWS Secret Access Key
How to get AWS access key: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey
---

## How to execute
1. clone git repository: **git clone https://pvampelj@bitbucket.org/pvampelj/myhomework.git**
2. move to the myhomework directory: **cd myhomework**
3. make sure Python3 is installed: **python3 --version**
4. create a virtual environment for python: **python3 -m venv venv**
5. activate the virtual environment: **source venv/bin/activate**
6. install the required python packages: **pip install -r requirements.txt**
7. execute the script: **python ./MyApp.py**
8. enter the requested parameters by the script
