#Imports
from __future__ import print_function

import json
import os
import sys
import time
import stat
import urllib.request
from pathlib import Path
import re
import boto3
import paramiko
from scp import SCPClient


#Functions
def create_ansible_server(aws_access_key, aws_secret_key, cf_client, my_pub_ip, aws_ssh_key, application_name):
    __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, 'Config/create_ansible_server.json')) as json_file:
        json_config = json.load(json_file)
        json_string = json.dumps(json_config)
   
    stack_parameters = []
    stack_parameters.append({"ParameterKey": "SSHLocation", "ParameterValue": my_pub_ip + "/32"})
    stack_parameters.append({"ParameterKey": "KeyName", "ParameterValue": aws_ssh_key})
    stack_parameters.append({"ParameterKey": "AWSAccessKeyID", "ParameterValue": aws_access_key})
    stack_parameters.append({"ParameterKey": "AWSSecretAccessKey", "ParameterValue": aws_secret_key})
    
    #Create stack
    print("Starting to create ansible server")
    result = cf_client.create_stack(StackName=application_name + "-ansible-server", DisableRollback=True, TemplateBody=json_string, Parameters=stack_parameters)
    print("-"*65)
    print("Calling CREATE_STACK method to create: " + application_name + "-ansible-server")
    cur_staus = ""
    print("Output from API call: ")
    print(result)
    print(" ")
    cur_status = check_status(cf_client, application_name + "-ansible-server")
    if cur_status == "CREATE_COMPLETE":
        print("Stack " + application_name + "-ansible-server created successfully.")
        return True
    else:
        print("Failed to create stack " + application_name + "-ansible-server")
        sys.exit(1)


def update_ansible_server(aws_access_key, aws_secret_key, cf_client, my_pub_ip, aws_ssh_key, application_name):
    __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, 'Config/create_ansible_server.json')) as json_file:
        json_config = json.load(json_file)
        json_string = json.dumps(json_config)
   
    stack_parameters = []
    stack_parameters.append({"ParameterKey": "SSHLocation", "ParameterValue": my_pub_ip + "/32"})
    stack_parameters.append({"ParameterKey": "KeyName", "ParameterValue": aws_ssh_key})
    stack_parameters.append({"ParameterKey": "AWSAccessKeyID", "ParameterValue": aws_access_key})
    stack_parameters.append({"ParameterKey": "AWSSecretAccessKey", "ParameterValue": aws_secret_key})
    
    #Update stack
    print("Starting to create ansible server")
    try:
        result = cf_client.update_stack(StackName=application_name + "-ansible-server", DisableRollback=True, TemplateBody=json_string, Parameters=stack_parameters)
    except Exception as ex:
        if(re.search("No updates are to be performed.", str(ex))):
            print("No updates to the ansible stack, will continue to update the app stack")
            return True
        else:
            print("Failed to update stack " + application_name + "-ansible-server")
            sys.exit(1)

    print("-"*65)
    print("Calling UPDATE_STACK method to create: " + application_name + "-ansible-server")
    cur_staus = ""
    print("Output from API call: ")
    print(result)
    print(" ")
    cur_status = check_status(cf_client, application_name + "-ansible-server")
    if cur_status == "UPDATE_COMPLETE":
        print("Stack " + application_name + "-ansible-server updated successfully.")
        return True
    else:
        print("Failed to update stack " + application_name + "-ansible-server")
        sys.exit(1)

def check_status(cf_client, stack_name):
    stacks = cf_client.describe_stacks(StackName=stack_name)["Stacks"]
    stack = stacks[0]
    cur_status = stack["StackStatus"]
    print("Current status of stack " + stack["StackName"] + ": " + cur_status)
    for n_loop in range(1, 9999):
        if "IN_PROGRESS" in cur_status:
            print("Waiting for status update(" + str(n_loop) + ")...", end="\r")
            time.sleep(1) # pause 1 seconds

            try:
                stacks = cf_client.describe_stacks(StackName=stack_name)["Stacks"]
            except Exception as e:
                print(" ")
                print("Stack " + stack["StackName"] + " no longer exists")
                print(e)
                cur_status = "STACK_DELETED"
                break

            stack = stacks[0]

            if stack["StackStatus"] != cur_status:
                cur_status = stack["StackStatus"]
                print(" ")
                print("Updated status of stack " + stack["StackName"] + ": " + cur_status)
        else:
            break

    return cur_status

def transfer_files_to_host(remote_host, ssh_key_file, local_location, remote_location):
    ssh_key = paramiko.RSAKey.from_private_key_file(ssh_key_file)
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys(ssh_key_file)
    ssh.connect( hostname = remote_host, username = "ec2-user", pkey = ssh_key)

    scp = SCPClient(ssh.get_transport())
    scp.put(local_location, recursive=True, remote_path=remote_location)
    scp.close()

def transfer_files_from_host(remote_host, ssh_key_file, remote_location, local_location):
    ssh_key = paramiko.RSAKey.from_private_key_file(ssh_key_file)
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys(ssh_key_file)
    ssh.connect( hostname = remote_host, username = "ec2-user", pkey = ssh_key)

    scp = SCPClient(ssh.get_transport())
    scp.get(remote_location, local_path=local_location)
    scp.close()
    

def execute_ssh_command(remote_host, ssh_key_file, command):
    ssh_key = paramiko.RSAKey.from_private_key_file(ssh_key_file)
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys(ssh_key_file)
    ssh.connect( hostname = remote_host, username = "ec2-user", pkey = ssh_key, timeout=1500, )
    
    print('Remote output started \n\n')
    stdin, stdout, stderr = ssh.exec_command(command, get_pty=True)
    for line in iter(stdout.readline, ""):
        print(line, end="")
    print('Remote output stopped \n\n')

def createAnsibleSSHKey(application_name, aws_region, aws_access_key, aws_secret_key):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    key_present = os.path.isfile(f"{dir_path}/outputs/{application_name}-ansible-server.pem")

    if(key_present):
        return f"outputs/{application_name}-ansible-server.pem"
    else:
        ec2 = boto3.client('ec2', aws_region, aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key)
        key = ec2.create_key_pair(KeyName=f"{application_name}-ansible-server")
        f = open(f"{dir_path}/outputs/{application_name}-ansible-server.pem", "w")
        f.write(key['KeyMaterial'])
        f.close()
        return f"{dir_path}/outputs/{application_name}-ansible-server.pem"

def transfer_files_from_host(remote_host, ssh_key_file, remote_location, local_location):
    ssh_key = paramiko.RSAKey.from_private_key_file(ssh_key_file)
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys(ssh_key_file)
    ssh.connect( hostname = remote_host, username = "ec2-user", pkey = ssh_key)

    scp = SCPClient(ssh.get_transport())
    scp.get(remote_location, local_path=local_location)
    scp.close()

def getELBDNS(application_name, aws_region, aws_access_key, aws_secret_key, lb_name):
    elb = boto3.client('elbv2', aws_region, aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key)
    print(lb_name)
    elb_list = elb.describe_load_balancers(Names=[f"{lb_name}"])
    return elb_list['LoadBalancers'][0]['DNSName']

#End Functions


#Main program
def main(aws_access_key, aws_secret_key, aws_region ,application_name, my_pub_ip, db_user, db_pass, debug_deploy):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print(f"Working in {dir_path}")
    aws_ssh_key_path = createAnsibleSSHKey(application_name, aws_region, aws_access_key, aws_secret_key)
    aws_ssh_key_name = f"{application_name}-ansible-server"
    cf_client = boto3.client('cloudformation', aws_region, aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key)
    
    ansible_extra_param = ""
    if(re.match('YES', debug_deploy, re.IGNORECASE)):
        ansible_extra_param += "-vvv" 


    #Check if any application stacks already exist
    stack_list = cf_client.describe_stacks()["Stacks"]
    stack_exists = False
    ansible_stack_exists = False
    for stack in stack_list:
        if application_name + "-ansible-server" == stack["StackName"]:
            print("Stack " + application_name + " already exists. Will update the stack")
            stack_exists = True
        if application_name + "-ansible-server" == stack["StackName"]:
            print("Stack " + application_name + "-ansible-server already exists. Will update the stack")
            ansible_stack_exists = True


    #If the stack already exists then update it
    if (stack_exists or ansible_stack_exists):
        result = update_ansible_server(aws_access_key, aws_secret_key, cf_client, my_pub_ip, aws_ssh_key_name, application_name)
    #Else: create the stack
    else:
        result = create_ansible_server(aws_access_key, aws_secret_key, cf_client, my_pub_ip, aws_ssh_key_name, application_name)
    if(result):
        ansible_folder = "config/ansible/"
        remote_locaion = "/home/ec2-user/ansible/"
        remote_host = cf_client.describe_stacks(StackName=application_name+"-ansible-server")["Stacks"][0]["Outputs"][1]["OutputValue"]
        print("Ansible server can be reached at: " + remote_host)
        print("Transfering ansible config files...")
        transfer_files_to_host(remote_host, aws_ssh_key_path, ansible_folder, remote_locaion)
        print("Transfer complete.")

        #Execue ansible commands
        ansible_create_app = f"ansible-playbook {ansible_extra_param} ansible/deploy.yml --private-key ansible/{application_name}-ansible-server-private-key.pem --extra-vars \"region={aws_region} instance_type=t2.micro keypair={application_name}-ansible-server-key ssh_location={remote_host}/32 stack_name={application_name} db_user={db_user} db_pass={db_pass}\""
        print(f"Executting command: {ansible_create_app}")
        execute_ssh_command(remote_host, aws_ssh_key_path, ansible_create_app)

        print("Transfering application ssh key from ansible-server")
        try:
            transfer_files_from_host(remote_host, aws_ssh_key_path, f"/home/ec2-user/ansible/{application_name}-ansible-server-private-key.pem", f"{dir_path}/outputs/{application_name}-app-servers-private-key.pem")
            print("Transfer complete")
        except:
            print("Application ssh key already present")

        alb_dns = getELBDNS(application_name, aws_region, aws_access_key, aws_secret_key, f"ALB-{application_name}")

        print("Application stack upgraded / deployed\n")
        print(f"Your application is availible at: http://{alb_dns}\n")
        print("You can ssh to yor ansible server with:")
        print(f"ssh -i {aws_ssh_key_path} ec2-user@{remote_host}\n")
        os.chmod(aws_ssh_key_path, 400)
        status = oct(os.stat(aws_ssh_key_path).st_mode)[-3:]
        if(status != "400"):
            print("But you must first chmod the key file to 400 !!!\n\n")
            print(f"chmod 400 {aws_ssh_key_path}")
        print("STAY AWSOME!")
        

    #TODO: Desroy ansible stack

#Get user input & Call main program
if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    Path(f"{dir_path}/outputs").mkdir(parents=True, exist_ok=True)
    application_name = str(input("Application name (leave blank for 'MyApp')\n") or "MyApp")

    #Log
    class Logger(object):
        def __init__(self):
            self.terminal = sys.stdout
            self.log = open(f"outputs/{application_name}.log", "a")
    
        def write(self, message):
            self.terminal.write(message)
            self.log.write(message)  

        def flush(self):
            # this flush method is needed for python 3 compatibility.
            # this handles the flush command by doing nothing.
            # you might want to specify some extra behavior here.
            pass
    sys.stdout = Logger()

    #TODO Make an option to chose a file
    aws_access_key = str(input("AWS Access Key - required (e.g. ABCDE1FGHIJKL2MNOPQR)\n") or "AKIAR2BHYW75ZEBK6DVL")
    aws_secret_key = str(input("AWS Secret Access Key - required (e.g. aBCdE1fGHijKlMn+OPq2RsTUV3wxy45Zab6c+7D8)\n") or "Js8W4ZcWRFqWK+pynKVu5mpc6LrCgxnnMfN29k14")
    aws_region = str(input("AWS Region (leave blank for 'eu-central-1')\n") or "eu-central-1")
    
    db_user = str(input("Application DB user (leave blank for 'admin')\n") or "admin")
    db_pass = str(input("Application DB password (leave blank for 'ChangeMe321')\n") or "ChangeMe321")
    debug_deploy = str(input("Verbose ansible deployment? YES/NO (leave blank for 'NO')\n") or "NO")
    print("Getting your public IP address")
    my_pub_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')

    print("Your public IP is: {}".format(my_pub_ip))
    print(aws_access_key)
    print(aws_secret_key)
    print(aws_region)
    print(application_name)

    main(aws_access_key, aws_secret_key, aws_region ,application_name, my_pub_ip, db_user, db_pass, debug_deploy)